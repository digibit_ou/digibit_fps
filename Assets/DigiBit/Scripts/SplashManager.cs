﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SplashManager : MonoBehaviour
{
     public Image LogoImg;

     IEnumerator Start()
     {
          LogoImg.canvasRenderer.SetAlpha(0.0f);

          FadeIn();
          yield return new WaitForSeconds(2.0f);

          FadeOut();
          yield return new WaitForSeconds(1.0f);

          SceneManager.LoadScene("00-TutorialVideo");
     }

     void FadeIn()
     {
          LogoImg.CrossFadeAlpha(1.0f, 1.5f, false);
     }

     void FadeOut()
     {
          LogoImg.CrossFadeAlpha(0.0f, 1.5f, false);
     }
}