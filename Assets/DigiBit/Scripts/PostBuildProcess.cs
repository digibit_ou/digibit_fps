﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif
using System.IO;

public class PostProcessBuild
{
     [PostProcessBuild]
     public static void OnPostprocessBuild(BuildTarget buildTarget, string pathToBuiltProject)
     {
#if UNITY_IOS
          string pbxprojPath = pathToBuiltProject + "/Unity-iPhone.xcodeproj/project.pbxproj";

          PBXProject proj = new PBXProject();

          proj.ReadFromString(File.ReadAllText(pbxprojPath));

          string Target = proj.TargetGuidByName("Unity-iPhone");

          proj.AddFrameworkToProject(Target, "CoreBluetooth.framework", false);

          File.WriteAllText(pbxprojPath, proj.WriteToString());

          string plistPath = pathToBuiltProject + "/Info.plist";
          PlistDocument plist = new PlistDocument();
          plist.ReadFromString(File.ReadAllText(plistPath));

          PlistElementDict rootDict = plist.root;

        rootDict.SetString("NSBluetoothPeripheralUsageDescription", "Ble used to connect to DigiBit hardware.");
        rootDict.SetString("NSCameraUsageDescription", "Used to record gamplay and save video.");
        rootDict.SetString("NSPhotoLibraryUsageDescription", "Used to record gamplay and save video.");
        rootDict.SetString("NSMicrophoneUsageDescription", "Used to record gamplay and save video.");
        rootDict.SetString("NSPhotoLibraryAddUsageDescription", "Used to record gamplay and save video.");

          PlistElementArray bgModes = rootDict.CreateArray("LSApplicationQueriesSchemes");
          bgModes.AddString("digibitinfo");

          File.WriteAllText(plistPath, plist.WriteToString());
#endif
     }
}
#endif