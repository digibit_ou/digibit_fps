﻿//TO RUN THIS SCRIPT

//1) Change Build Platform to PC and uncomment #define PC_BUILD
//2) Add this script to an empty GameObject named "SerialPort"
//3) Change the API Compatibility Level from ".NET 2.0 Subset" to ".NET 2.0" in Player Settings.
//4) When no simulation needed comment
//#define PC_BUILD to change Build Platform without any other changes

//This works with Windows only for now

#if !(UNITY_IOS || UNITY_ANDROID)
using UnityEngine;
using System.Collections;
using System.IO;
using System.IO.Ports;
using System;
using DigiBitSDK;

public class SerialPortCommunication : MonoBehaviour
{
    private const string TAG = "Digibit-Serial-Port Data ";
    public String ComPort = "\\\\.\\COM21";
    public bool LogsEnabled = true;
    SerialPort serialPort;

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        serialPort = new SerialPort(ComPort, 1000000, Parity.None, 8, StopBits.One);

        if (serialPort.IsOpen)
        {
            serialPort.Close();
            Debug.LogError("Closing port, because it was already open!");
        }
        else
        {
            serialPort.ReadBufferSize = 42;
            serialPort.Open();
            serialPort.ReadTimeout = 30;
            ClearBuffer();

            Debug.Log("Port Opened!");
        }
    }

    void onApplicationQuit()
    {
        if (serialPort != null && serialPort.IsOpen)
        {
            // End PID data
            WriteString("E0100\n");
            serialPort.Close();
        }
        if (LogsEnabled)
            Debug.Log("Port closed!");

    }

    void Update()
    {
        if (serialPort == null || !serialPort.IsOpen)
            return;

        try
        {
            string ReadData = serialPort.ReadLine();
            UnityEditorCommunication.SerialInput(ReadData);
            //Debug.Log (TAG + ReadData);
            ClearBuffer();
        }
        catch (TimeoutException)
        {

        }
    }

    void WriteString(string data)
    {
        serialPort.Write(data);
    }

    void ClearBuffer()
    {
        try
        {
            serialPort.ReadLine();
            serialPort.ReadLine();
            serialPort.ReadLine();
            serialPort.ReadLine();
        }
        catch (TimeoutException)
        {

        }
    }
}
#else

using UnityEngine;
using System.Collections;
using System;

public class SerialPortCommunication : MonoBehaviour
{
	void Start()
	{
		Destroy (this);
	}
}


#endif
